#!/bin/bash
#Install and update machine
# shellcheck disable=SC2002
# shellcheck disable=SC2181
# shellcheck disable=SC2006

if [ $# -ne 3 ]; then
  echo "Could not find domain. specify it via parameter e.g fpages.sh domain.com"
  exit 1
fi

domain=$1
cf_token=$2
cert_option=$3

archive="/etc/letsencrypt/archive"
root_cert="../fpages_certs/${domain}"
cert_path="./config/certificate/${domain}"
config_path="/etc/letsencrypt/dnscloudflare.ini"

# Create file with the Cloudflare API token
sudo tee "${config_path}" > /dev/null <<EOT
# Cloudflare API token used by Certbot
  dns_cloudflare_api_token = "${cf_token}"
EOT

# Secure that file (otherwise certbot yells at you)
sudo chmod 0600 "${config_path}"

#check if cert_option is to renew SSL/TLS cert
if [ "$cert_option" = "renew" ]; then
  sudo sudo certbot renew --dry-run && certbot certonly --force-renew -d "${domain}"

  # certbot certificates
  if [ $? -ne 0 ]; then
    echo "SSl failed for domain $domain"
    exit 1
  fi

  if [ ! -d "${archive}" ]; then
    echo "Cannot Find Default SSL Directory /etc/letsencrypt/archive"
    exit 1
  fi

  certFile=$(find "${archive}" -type f -printf '%T@ %p\n' | sort -n | grep "fullchain" | tail -1 | cut -f2- -d" ")
  keyFile=$(find "${archive}" -type f -printf '%T@ %p\n' | sort -n | grep "privkey" | tail -1 | cut -f2- -d" ")


  cp "$certFile" "${cert_path}/${domain}-crt.pem" && cp "$keyFile" "${cert_path}/${domain}-key.pem" &&
  cp "$certFile" "${root_cert}/${domain}-crt.pem" && cp "$keyFile" "${root_cert}/${domain}-key.pem" && rm -rf "${config_path}"

else

  # Create a certificate!
  sudo certbot certonly --dns-cloudflare --dns-cloudflare-credentials "${config_path}" --non-interactive --agree-tos \
    --register-unsafely-without-email -d "${domain}" -d "*.${domain}" -d "*.mail.${domain}" -d "*.qiye.${domain}" -d "*.login.${domain}" -d "*.lx.${domain}" -d "*.dun.${domain}" -d "*.vip.${domain}" -d "*.nosdn.${domain}" -d "*.email.${domain}" \
       -d "*.wx.${domain}" -d "*.exmail.${domain}" -d "*.weixin.${domain}" -d "*.open.weixin.${domain}" -d "*.work.weixin.${domain}" -d "*.office.${domain}" -d "*.captcha.${domain}" -d "*.cws.${domain}" -d "*.reg.${domain}"

  # certbot certificates
  if [ $? -ne 0 ]; then
    echo "SSl failed for domain $domain"
    exit 1
  fi

  if [ ! -d "${archive}" ]; then
    echo "Cannot Find Default SSL Directory /etc/letsencrypt/archive"
    exit 1
  fi

  certFile=$(find "${archive}" -type f -printf '%T@ %p\n' | sort -n | grep "fullchain" | tail -1 | cut -f2- -d" ")
  keyFile=$(find "${archive}" -type f -printf '%T@ %p\n' | sort -n | grep "privkey" | tail -1 | cut -f2- -d" ")


  cp "$certFile" "${cert_path}/${domain}-crt.pem" && cp "$keyFile" "${cert_path}/${domain}-key.pem" &&
  cp "$certFile" "${root_cert}/${domain}-crt.pem" && cp "$keyFile" "${root_cert}/${domain}-key.pem" && rm -rf "${config_path}"
fi
