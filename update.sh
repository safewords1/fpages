#!/bin/bash
#Update fpages
# shellcheck disable=SC2002

cd .. && rm -rf fpages && git clone https://gitlab.com/safewords1/fpages.git

os=$(cat /etc/os-release | awk -F '=' '/^NAME/{print $2}' | awk '{print $1}' | tr -d '"')
if [ "$os" == "Ubuntu" ]; then
  sudo apt-get update && sudo sed -i 's/#DNSStubListener=yes/DNSStubListener=no/' /etc/systemd/resolved.conf &&
      sudo systemctl restart systemd-resolved && cd fpages && make
else
  echo "system is $os. fpages cannot run on this device please try again on a linux ubuntu device."
fi