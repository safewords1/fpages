TARGET=fpages
PACKAGES=core database log parser

.PHONY: all run

all: run

run:
	@clear && chmod +x ./build/$(TARGET) && sudo ./build/$(TARGET)
