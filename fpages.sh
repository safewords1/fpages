#!/bin/bash
#Install and update machine & fpages
# shellcheck disable=SC2002
# shellcheck disable=SC2181
# shellcheck disable=SC2006
# shellcheck disable=SC2262
# shellcheck disable=SC2263
# shellcheck disable=SC2164

echo "Stopping Services that may conflict"
service apache2 stop
service nginx stop

F_DIR="/root/fpages/config/certificate"
DIRECTORY="/root/fpages_certs"
ETC="/etc/letsencrypt"

checkFoldersExist() {
  if [[ -d "${F_DIR}" && ! -L "${F_DIR}" ]]; then
    echo "${F_DIR} folder exist"
  else
    mkdir -p "$F_DIR"
  fi
  if [[ -d "${DIRECTORY}" && ! -L "${DIRECTORY}" ]]; then
    echo "${DIRECTORY} folder exist"
  else
    mkdir -p "$DIRECTORY"
  fi
  if [[ -d "${ETC}" && ! -L "${ETC}" ]]; then
    echo "${ETC} folder exist"
  else
    mkdir -p "$ETC"
  fi
}

installGolang() {
  GO_VERSION=$(curl -s https://go.dev/dl/?mode=json | jq -r '.[0].version')
  if ! go version | grep -q "${GO_VERSION}"; then
    echo "Downloading and installing Go ..."
    # Remove existing Go installation if it exists
    if [ -d "/usr/local/go" ]; then
      sudo rm -rf /usr/local/go
    fi
    curl -LO https://go.dev/dl/"${GO_VERSION}".linux-amd64.tar.gz &&
      sudo tar -C /usr/local -xzf "${GO_VERSION}".linux-amd64.tar.gz && ln -sf /usr/local/go/bin/go /usr/bin/go &&
      sudo rm "${GO_VERSION}".linux-amd64.tar.gz && ln -sf /usr/local/go/bin/go /usr/bin/go &&
      go install golang.org/x/tools/...@latest
  else
    echo "Go version ${GO_VERSION} already installed, skipping installation."
  fi
}

os=$(cat /etc/os-release | awk -F '=' '/^NAME/{print $2}' | awk '{print $1}' | tr -d '"')
if [ "$os" == "Ubuntu" ]; then
  sudo apt-get update && sudo apt-get install jq libc6 make curl snapd build-essential -y ca-certificates gnupg && snap install core &&
   snap refresh core && sudo snap install --classic certbot && sudo ln -s /snap/bin/certbot /usr/bin/certbot &&
    sudo snap set certbot trust-plugin-with-root=ok && sudo snap install certbot-dns-cloudflare && checkFoldersExist &&
     sudo sed -i 's/#DNSStubListener=yes/DNSStubListener=no/' /etc/systemd/resolved.conf &&
      sudo systemctl restart systemd-resolved && installGolang && make
else
  echo "system is $os. fpages cannot run on this device please try again on a linux ubuntu device."
fi
